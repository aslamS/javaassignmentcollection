package com.java;

import java.util.Scanner;

public class Strclass1 {
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter the String");
		String s1 = s.next().toLowerCase().trim();
		String s2 = "";
		for (int i = s1.length() - 1; i >= 0; i--) {
			s2 = s2 + s1.charAt(i);
		}
		if (s1.equals(s2)) {
			System.out.println("Given String is Palindrome ");
		} else {
			System.out.println("Given String is not palindrome");
		}
	}
}
